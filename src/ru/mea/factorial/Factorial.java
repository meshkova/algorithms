package ru.mea.factorial;
/**
 * Класс, демонстрирующий поиск факториала используя класс BigInteger
 *
 * @author Е.А.Мешкова 18ИТ18
 */

import java.util.Scanner;
import java.math.*;

public class Factorial {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число, факториал которого хотите найти");
        int fact = scanner.nextInt();
        if (fact < 0) System.out.println("Факториала отрицательного числа нет");
        System.out.println(factorial(fact));
    }

    /**
     * Метод, вычисляющий факториал
     *
     * @param fact целочисленное неотрицательное число
     * @return возвращает факториал
     */
    private static BigInteger factorial(int fact) {
        if (fact == 0 || fact == 1) return BigInteger.ONE;
        BigInteger composition = BigInteger.ONE;
        for (int i = 2; i <= fact; i++) {
            composition = composition.multiply(BigInteger.valueOf(i));

        }

        return composition;
    }
}
