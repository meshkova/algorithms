package ru.mea.recursion;

public class Recursion {
    public static void main(String[] args) {
        System.out.println(factorial(4));
    }

    private static long factorial(int value) {
        if (value == 0 || value == 1) {
            return 1;
        }
        return factorial(value - 1) * value;
    }

}
