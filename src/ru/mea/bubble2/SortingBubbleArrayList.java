package ru.mea.bubble2;

/**
 * Класс, демонстрирующий использование сортировки пузырьком в ArrayList
 *
 * @author Е.А.Мешкова 18ИТ18
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SortingBubbleArrayList {
    public static void main(String[] args) {
        List<Integer> array = new ArrayList<>(Arrays.asList(-1, 2, 3, 88));
        sort(array);
        for (Integer integer : array) System.out.print(integer + " ");

    }

    /**
     * Сортировка целых чисел по убыванию
     *
     * @param array ArrayList
     */
    private static void sort(List<Integer> array) {
        for (int i = 0; i < array.size(); i++) {
            boolean isSort = false;
            for (int j = 0; j < array.size() - 1; j++) {
                if (array.get(j) < array.get(j + 1)) {
                    isSort = true;
                    int tmp = array.get(j);
                    array.set(j, array.get(j + 1));
                    array.set(j + 1, tmp);
                }

            }
            if (!isSort) {
                System.out.println("массив пройден ");
                break;
            }


        }

    }
}
