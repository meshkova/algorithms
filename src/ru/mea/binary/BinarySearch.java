package ru.mea.binary;

import java.util.*;

/**
 * Класс, демонстрирующий бинарный поиск
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class BinarySearch {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array = new int[5];
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 90) - 5);
            System.out.print(array[i] + " ");
        }
        List<Integer> twoArray = new ArrayList<>(Arrays.asList(0, 7, 78, 7, 3, 88, 5, 7, 11));
        System.out.println("Сортированный массив");
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        System.out.println("Введите число");
        int element = scanner.nextInt();
        System.out.println(binary(array, element));
        Collections.sort(twoArray);
        System.out.println(Arrays.toString(new List[]{twoArray}));
        System.out.println(binaryTwo(twoArray, element));

    }

    /**
     * @param array   массив
     * @param element элемент массива
     * @return идекс элемента массива,
     * если же такого элемента нет, то -1
     */
    private static int binary(int[] array, int element) {
        int mid;
        int first = 0;
        int last = array.length - 1;
        do {
            mid = (last + first) / 2;
            int guess = array[mid];
            if (guess == element) return mid;

            if (guess < element) {
                first = mid + 1;
            } else {
                last = mid - 1;
            }
        } while (first <= last);

        return -1;
    }

    /**
     * @param twoArray ArrayList
     * @param element  элемент массива
     * @return идекс элемента массива,
     * если же такого элемента нет, то -1
     */
    private static int binaryTwo(List<Integer> twoArray, int element) {
        int mid;
        int first = twoArray.get(0);
        int last = twoArray.size() - 1;
        do {
            mid = (last + first) / 2;
            int guess = twoArray.get(mid);
            if (guess == element) return mid;

            if (guess < element) {
                first = mid + 1;
            } else {
                last = mid - 1;
            }
        } while (first <= last);

        return -1;
    }
}

