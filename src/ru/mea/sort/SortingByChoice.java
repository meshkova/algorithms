package ru.mea.sort;

/**
 * Сортировка выбором
 *
 * @author Е.А.Мешкова 18ИТ18
 */

public class SortingByChoice {


    public static void main(String[] args) {
        int[] array = new int[5];
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 90) - 5);
            System.out.print(array[i] + " ");

        }

        System.out.println("Отсортированный массив: ");
        sort(array);
        for (int value : array) {
            System.out.print(value + " ");
        }

    }

    /**
     * Сортирует элементы массива по возрастанию
     * Метод, который размещает целые, положительные числа по возрастанию
     *
     * @param array массив
     */
    private static void sort(int[] array) {

        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int mini = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    mini = j;
                }
            }
            if (i != mini) {
                int tmp = array[i];
                array[i] = array[mini];
                array[mini] = tmp;
            }

        }
    }

}







