package ru.mea.fibonacci;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Класс, демонстрирующий способ нахождения n-го числа Фибоначчи
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class FibonacciNumbers {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите порядковый номер");
        int num = scanner.nextInt();
        long before = System.currentTimeMillis();
        System.out.println(fibonacci(num));
        long after = System.currentTimeMillis();
        System.out.println(after - before + " миллисекунд, требуется итеративному методу");
        long beforeStart = System.currentTimeMillis();
        System.out.println(fibo(num));
        long afterEnd = System.currentTimeMillis();
        System.out.println(afterEnd - beforeStart + "  миллисекунд, требуется рекурсивному методу");
        // long
        System.out.println(fibonacciTwoMethod(num));
        // BigInteger
        System.out.println(fibonacciBig(num));

    }

    /**
     * Итеративный способ
     *
     * @param num Порядковый номер
     * @return число Фибоначчи
     */
    private static int fibonacci(int num) {
        int[] term = new int[num];
        term[0] = 1;
        term[1] = 1;

        for (int i = 2; i < num; i++) {
            term[i] = term[i - 1] + term[i - 2];

        }

        return term[num - 1];
    }

    /**
     * Рекурсивный способ
     *
     * @param num Порядковый номер
     * @return число Фибоначчи
     */
    private static int fibo(int num) {
        if (num == 1 || num == 2) {
            return 1;
        }
        return fibo(num - 1) + fibo(num - 2);
    }

    /**
     * Итеративный способ
     *
     * @param num Порядковый номер
     * @return число Фибоначчи типа long
     */
    private static long fibonacciTwoMethod(int num) {
        long[] term = new long[num];
        term[0] = 1;
        term[1] = 1;

        for (int i = 2; i < num; i++) {
            term[i] = term[i - 1] + term[i - 2];

        }

        return term[num - 1];
    }

    /**
     * Итеративный способ
     *
     * @param num Порядковый номер
     * @return число Фибоначчи типа BigInteger
     */
    private static BigInteger fibonacciBig(int num) {
        int[] term = new int[num];
        term[0] = 1;
        term[1] = 1;
        for (int i = 2; i < num; i++) {
            term[i] = term[i - 1] + term[i - 2];
        }
        return BigInteger.valueOf(term[num - 1]);
    }

}
