package ru.mea.java;

import java.util.Scanner;

/**
 * Задача данного класса связана с формулой
 * "квадрат гипотенузы равен сумме квадратов катета"
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class Java {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n, x, y, z;
        System.out.println("Введите n: ");
        n = scanner.nextInt();
        x = 1;
        while (x < n) {
            y = x;
            while (y <= n) {
                z = y;
                while (z <= n) {
                    if ((z <= x * 2) && (y <= x * 2) && (z * z == x * x + y * y)) {
                        System.out.println(x + " " + y + " " + z);
                    }
                    z++;
                }
                y++;

            }
            x++;
        }

    }

}
