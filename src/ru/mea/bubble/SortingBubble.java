package ru.mea.bubble;

/**
 * Сортировка пузырьком
 *
 * @autor Е.А.Мешкова 18ИТ18
 */

public class SortingBubble {
    public static void main(String[] args) {
        int[] array = new int[5];
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 90) + 5);
            System.out.print(array[i] + " ");
        }
        System.out.print("Отсортированный массив: ");
        sort(array);
        for (int value : array) {
            System.out.print(value + " ");
        }
    }

    /**
     * Сортировка целых чисел по возрастанию
     *
     * Проверяет попарно все элементы от начала
     * и перемещает меньший элемент(из пары)справа налево
     *
     *
     * @param array обычный массив
     */
    private static void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            boolean tmp = false;
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    tmp = true;
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
            if (!tmp) {
                System.out.println("массив пройден ");
                break;
            }

        }

    }
}


