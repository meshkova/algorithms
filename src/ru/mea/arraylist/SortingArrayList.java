package ru.mea.arraylist;
/**
 * Сортировка выбором, через ArrayList
 *
 * @autor Е.А.Мешкова 18ИТ18
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SortingArrayList {
    public static void main(String[] args) {
        List<Integer> array2 = new ArrayList<>(Arrays.asList(2, 7, 78, 7, 3, 88, 5, 7, 11));
        System.out.println("Числа по убыванию: ");
        sort(array2);
    }

    /**
     * Метод сортирует целые числа по убыванию
     *
     * @param array2 Автоматически расширяемый массив
     */
    private static void sort(List<Integer> array2) {
        for (int i = 0; i < array2.size(); i++) {

            int min = array2.get(i);
            int mini = i;
            for (int j = i + 1; j < array2.size(); j++) {
                if (min < array2.get(j)) {
                    min = array2.get(j);
                    mini = j;
                }
            }
            if (i != mini) {
                int tmp = array2.get(i);
                array2.set(i, min);
                array2.set(mini, tmp);
            }
            System.out.print(array2.get(i) + " ");
        }
    }
}
