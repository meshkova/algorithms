package ru.mea.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Класс, демонстрирующий сортировки по разным признакам
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class Demo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Person myxina = new Person("Мухина", 2003);
        Person ivanov = new Person("Иванов", 2002);
        Person yanina = new Person("Янина", 2003);
        Person lenina = new Person("Ленина", 1995);
        Person myxina2004 = new Person("Мухина", 2004);
        Person[] array = {ivanov, myxina, lenina, yanina, myxina2004};
        ArrayList<Person> arrayTwo = new ArrayList<>();
        System.out.println("Введите фамилию");
        String people = scanner.nextLine();
        sortSort(array, people, arrayTwo);
        person(arrayTwo, people);
        System.out.println("Отсортированно в алфавитном порядке");
        sort(array);
        for (Person value : array) {
            System.out.print(value + " ");
        }
        System.out.println();
        System.out.println("Информация о введённом человеке");
        System.out.println();
        System.out.println(binary(array, people));
        System.out.println();
        System.out.println("В порядке убывания");
        sortYear(array);
        for (Person in : array) {
            System.out.print(in + " ");
        }


    }

    /**
     * Метод, выполняющий сортировку по фамилиям
     * в алфавитном порядке
     *
     * @param array список людей
     */
    private static void sort(Person[] array) {
        for (int i = 0; i < array.length; i++) {
            boolean tmp = false;
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j].isSurname().compareTo(array[j + 1].isSurname()) > 0) {

                    tmp = true;
                    Person temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
            if (!tmp) {
                break;
            }

        }

    }

    /**
     * Метод, выполняющий сортировку по годам рождения
     * в порядке убывания
     *
     * @param array список
     */
    private static void sortYear(Person[] array) {
        for (int i = 0; i < array.length; i++) {
            boolean tmp = false;
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j].getYear() > array[j + 1].getYear()) {
                    tmp = true;
                    Person temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
            if (!tmp) {
                break;
            }

        }

    }

    /**
     * Метод, выполняющий поиск конкретного человека по фамилии
     *
     * @param array  список
     * @param people Введенная фамилия человека
     * @return null если человека с такой фамилией нет
     */
    private static Person binary(Person[] array, String people) {
        int mid;
        int first = 0;
        int last = array.length - 1;
        do {
            mid = (last + first) / 2;
            String guess = array[mid].isSurname();

            if (guess.equalsIgnoreCase(people)) {
                return array[mid];
            }

            if (guess.compareToIgnoreCase(people) == -1) {
                first = mid + 1;
            } else {
                last = mid - 1;
            }
        } while (first <= last);

        return null;
    }

    /**
     * @param array    список
     * @param people   фамилия человека
     * @param arrayTwo список людей с заданной фамилией
     */
    private static void sortSort(Person[] array, String people, List<Person> arrayTwo) {

        for (Person person : array) {
            if (people.equalsIgnoreCase(String.valueOf(person.isSurname()))) {
                arrayTwo.add(person);
            }
        }

    }

    /**
     * @param arrayTwo список людей с заданной фамилией
     * @param people   фамилия человека введённая с клавиатуры
     */
    private static void person(List<Person> arrayTwo, String people) {
        for (Person person : arrayTwo) {
            if (person.isSurname().equalsIgnoreCase(people)) {
                System.out.println(person);

            }

        }
        if (arrayTwo.size() == 0) {

            System.out.println("Таких фамилий нет");
        }

    }
}


