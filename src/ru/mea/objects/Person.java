package ru.mea.objects;

public class Person {
    private String surname;
    private int year;

    public Person(String surname, int year) {
        this.surname = surname;
        this.year = year;
    }

    public Person() {
        this("не указана", 0);
    }

    public String isSurname() {
        return surname;
    }

    public int getYear() {
        return year;
    }


    @Override
    public String toString() {
        return "Person{" +
                "surname='" + surname + '\'' +
                ", year=" + year +
                '}';
    }

}
