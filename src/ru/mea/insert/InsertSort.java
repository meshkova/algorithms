package ru.mea.insert;
/**
 * Класс, демонстрирующий сортировку вставками в ArrayList
 *
 * @author Е.А.Мешкова 18ИТ18
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InsertSort {
    public static void main(String[] args) {
        int[] array = new int[4];
        for (int i = 0; i < array.length; i++) {
            array[i] = ((int) (Math.random() * 90) + 5);
        }
        List<Integer> arrayTwo = new ArrayList<>(Arrays.asList(2, 7, 78, 7, 3, 88, 5, 7, 11));

        System.out.print(Arrays.toString(array));
        System.out.print(" Отсортированный массив по возрастанию: ");
        insertionSort(array);
        System.out.println(Arrays.toString(array));
        System.out.print(Arrays.toString(new List[]{arrayTwo}));
        System.out.print(" Отсортированный массив по убыванию: ");
        insertionSort(arrayTwo);
        System.out.println(Arrays.toString(new List[]{arrayTwo}));

    }

    /**
     * Сортирует массив {@code array} по возрастанию значений
     *
     * @param array массив целых чисел
     */
    private static void insertionSort(int[] array) {
        for (int out = 1; out < array.length; out++) {
            int temp = array[out];
            int in = out;
            while (in > 0 && array[in - 1] >= temp) {
                array[in] = array[in - 1];
                in--;
            }
            array[in] = temp;
        }
    }

    /**
     * Метод, сортирующий массив (@code arrayTwo) по убыванию
     *
     * @param arrayTwo ArrayList
     */
    private static void insertionSort(List<Integer> arrayTwo) {
        for (int out = 1; out < arrayTwo.size(); out++) {
            int temp = arrayTwo.get(out);
            int in = out;
            while (in > 0 && arrayTwo.get(in - 1) <= temp) {
                arrayTwo.set(in, arrayTwo.get(in - 1));
                in--;
            }
            arrayTwo.set(in, temp);
        }
    }
}
