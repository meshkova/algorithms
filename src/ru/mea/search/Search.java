package ru.mea.search;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Search {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        List<Integer> array2 = new ArrayList<>(Arrays.asList(0, 7, 78, 7, 3, 88, 5, 7, 11));
        int[] array1 = new int[12];
        for (int i = 0; i < array1.length; i++) {
            array1[i] = (int) (Math.random() * 90);
        }

        System.out.println("Введите число которое хотите найти");
        int givenNumber = scanner.nextInt();

        int num = given(array1, givenNumber);
        if (num == -1) {
            System.out.println("Такого числа в обычном массиве нет ");
        } else {
            System.out.println("Индекс искомого числа в обычном массиве: " + num);
        }
        int number = given2(array2, givenNumber);
        if (number == -1) {
            System.out.println("Такого числа нет");
        } else {
            System.out.println("Индекс искомого числа в ArrayList: " + number);
        }

    }

    /**
     * Поиск индекса искомого числа в обычном массиве
     *
     * @param array1      массив
     * @param givenNumber искомое число
     * @return возвращает индекс
     */
    private static int given(int[] array1, int givenNumber) {
        int index = -1;
        for (int i = 0; i < array1.length; i++) {
            if (givenNumber == array1[i]) return i;


        }
        return index;
    }

    /**
     * поиск индекса в ArrayList
     *
     * @param array2      ArrayList
     * @param givenNumber искомое число
     * @return возвращает индекс
     */
    private static int given2(List<Integer> array2, int givenNumber) {
        int index = -1;
        for (int i = 0; i < array2.size(); i++) {
            if (givenNumber == array2.get(i)) return i;


        }
        return index;
    }
}